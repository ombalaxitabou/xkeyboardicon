#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xcb/xcb.h>
#include <xcb/xkb.h>

#include "xkb.h"

static xcb_atom_t
get_atom (xcb_connection_t *connection, const char *atom_name)
{
	xcb_intern_atom_cookie_t atom_cookie;
	xcb_intern_atom_reply_t *atom_reply;
	xcb_generic_error_t *error;
	xcb_atom_t atom;

	atom_cookie = xcb_intern_atom (
			connection,
			1, /* only if exists */
			strlen(atom_name),
			atom_name);
	atom_reply = xcb_intern_atom_reply (
			connection,
			atom_cookie,
			&error);
	if ((!atom_reply) || (error))
	{
		fprintf (stderr, "Could not get %s atom\n", atom_name);
		return 0;
	}

	atom = atom_reply->atom;
	free (atom_reply);

	return atom;
}

uint8_t
init_xkb (xcb_connection_t *conn, xcb_window_t root_win, kb_t *kb)
{
	const xcb_query_extension_reply_t *ext;
	xcb_xkb_use_extension_cookie_t use_ext;
	xcb_xkb_use_extension_reply_t *use_ext_reply;
	xcb_xkb_get_controls_cookie_t controls;
	xcb_xkb_get_controls_reply_t *controls_reply;
	xcb_xkb_get_state_cookie_t state;
	xcb_xkb_get_state_reply_t *state_reply;
	xcb_atom_t xkb_rules_names_atom;
	xcb_get_property_cookie_t property;
	xcb_get_property_reply_t *property_reply;
	xcb_void_cookie_t select;
	xcb_generic_error_t *error;

	void *data;
	int length, i;
	char *p, *q, *end;

	/* Check if our server has XKB */
	ext = xcb_get_extension_data (conn, &xcb_xkb_id);
	if (!ext)
	{
		fprintf (stderr, "No XKB in X server\n");
		return 0;
	}

	/* X server's XKB extension is compatible */
	use_ext = xcb_xkb_use_extension (
			conn,
			XCB_XKB_MAJOR_VERSION,
			XCB_XKB_MINOR_VERSION);
	use_ext_reply = xcb_xkb_use_extension_reply(conn, use_ext, NULL);
	if ((!use_ext_reply) || (!use_ext_reply->supported))
	{
		fprintf (stderr, "Couldn't use XKB extension\n");
		return 0;
	}
	free (use_ext_reply);

	/* Find out number of different layouts */
	controls = xcb_xkb_get_controls (conn, XCB_XKB_ID_USE_CORE_KBD);
	controls_reply = xcb_xkb_get_controls_reply (conn, controls, &error);
	if ((!controls_reply) || (error))
	{
		fprintf (stderr, "Could not get XKB controls\n");
		return 0;
	}

	kb->numGroups = controls_reply->numGroups;
	free (controls_reply);

	/* Querry current layout */
	state = xcb_xkb_get_state (conn, XCB_XKB_ID_USE_CORE_KBD);
	state_reply = xcb_xkb_get_state_reply (conn, state, &error);
	if ((!state_reply) || (error))
	{
		fprintf (stderr, "Could not get XKB state\n");
		return 0;
	}

	kb->curGroup = state_reply->group;
	free (state_reply);

	/* Get layout names */
	xkb_rules_names_atom = get_atom (conn, "_XKB_RULES_NAMES");
	if (!xkb_rules_names_atom)
	{
		return 0;
	}

	property = xcb_get_property (
			conn,
			0, /* do not delete */
			root_win,
			xkb_rules_names_atom,
			XCB_ATOM_STRING,
			0,
			1024);
	property_reply = xcb_get_property_reply (
			conn,
			property,
			&error);
	if ((!property_reply)
	|| (property_reply->type != XCB_ATOM_STRING)
	|| (property_reply->bytes_after > 0)
	|| (property_reply->format != 8)
	|| (error))
	{
		fprintf (stderr, "Could not get _XKB_RULES_NAMES atom string\n");
		return 0;
	}

	data = xcb_get_property_value (property_reply);
	length = xcb_get_property_value_length (property_reply);

	/* Find the first character of layout strings in reply data and copy it */
	p = (char *) data;
	end = p + length;
	for (i = 0; p < end && i < 2; p++) if (*p == '\0') i++;

	for (i = 0; i < LAYOUT_MAX && i < kb->numGroups && p < end; i++, p = q + 1)
	{
		for (q = p; *q != ',' && *q != '\0'; q++);
		kb->groupNames[i] = strndup (p, q - p);
	}

	free(property_reply);

	/* Request XKB events */
	select = xcb_xkb_select_events_checked (
			conn,
			XCB_XKB_ID_USE_CORE_KBD,
			XCB_XKB_EVENT_TYPE_STATE_NOTIFY,
			0,
			XCB_XKB_EVENT_TYPE_STATE_NOTIFY,
			XCB_XKB_STATE_PART_GROUP_STATE,
			XCB_XKB_STATE_PART_GROUP_STATE,
			NULL); /* details */

	error = xcb_request_check (conn, select);
	if (error)
	{
		fprintf (stderr, "Could not select XKB events\n");
		return 0;
	}

	/* Everything is succesfull */
	return ext->first_event;
}

int
handle_xkb_event (xcb_generic_event_t *event, kb_t *kb)
{
	union xkb_event
	{
		struct
		{
			uint8_t response_type;
			uint8_t xkbType;
			uint16_t sequence;
			xcb_timestamp_t time;
			uint8_t deviceID;
		} any;
		xcb_xkb_state_notify_event_t state_notify;
	} *xkb_event;

	xkb_event = (union xkb_event *) event;
	switch (xkb_event->any.xkbType)
	{
		case XCB_XKB_STATE_NOTIFY:
			if (kb->curGroup != xkb_event->state_notify.group)
			{	/* layout changed */
				kb->curGroup = xkb_event->state_notify.group;
				return 1;
			}
			return 0;

		default:
			return 0;
	}
}
