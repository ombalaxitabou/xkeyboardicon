#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memset () */

#include <xcb/xcb.h>
#include <xcb/xcb_event.h> /* xcb_event_get_label () */

#include <xti.h>
#include <xpmb.h>

#include "xkb.h"
#include "flags.h"

#define BUFFER_MAX 1000

/* This should be done during compile */
#ifndef ICON_PATH
#define ICON_PATH "/usr/local/share/xkeyboardicon/flags/%d/%s"
#endif

char		 bg[8];
unsigned int	 sec;

/*
 * Choose best icon size for window with w width and h height
 */
int
select_icon_size (int w, int h)
{ /* suppose icons are squares */
	int i, s, m;

	if (w < h)
		m = w;
	else
		m = h;

	for (s = flag_size[0], i = 1; i < flag_size_count; i++)
	{
		if (((flag_size[i] <= m) && (flag_size[i] > s))
		|| ((s > m) && (flag_size[i] <= s)))
			s = flag_size[i];
	}

	return s;
}

unsigned char *
get_image_data (char *name, int size, int *width, int *height)
{
	unsigned char *image = NULL;
	char buffer[BUFFER_MAX];
	char *p, *b = NULL;
	char **d;
	xpm_image_t *xpm = NULL;
	int ret;

	if (!name || size < 0)
	{
		return NULL;
	}

	snprintf (
			buffer,
			BUFFER_MAX - 4,
			ICON_PATH,
			size,
			name);

	for (p = buffer; *p != '\0'; p++);
	strcpy (p, ".xpm");

	ret = read_file_to_buffer (buffer, &b);
	if (ret)
	{
		fprintf (stderr, "failed to load file into buffer\n");
		return NULL;
	}

	ret = parse_buffer_to_xpm_data (b, &d);
	free (b);
	if (ret)
	{
		fprintf (stderr, "failed to parse buffer to xpm data\n");
		return NULL;
	}

	ret = parse_xpm_data_to_xpm_image (d, &xpm);
	if (ret)
	{
		fprintf (stderr, "failed to parse xpm data\n");
		free_xpm_data (&d);
		return NULL;
	}

	*width = xpm_image_get_colums (xpm);
	*height = xpm_image_get_rows (xpm);

	if (bg[0])
	{
		xpm_image_set_transparent_color (xpm, bg);
	}

	ret = parse_xpm_image_pixel_data (xpm, &image);
	free_xpm_image (xpm);
	free_xpm_data (&d);
	if (ret)
	{
		fprintf (stderr, "failed to create pixel data\n");
		return NULL;
	}

	return image;
}

int
switch_icon (x_t *x, kb_t *kb)
{
	int width, height, size;
	unsigned char *image = NULL;

	/* Try cache first */
	if (xti_draw_icon_from_cache (x, kb->curGroup) == 0)
	{
		return 0;
	}

	/* This will set width and height to window's width and height */
	if (xti_get_window_size (
			xti_get_connection (x),
			xti_get_tray_icon_window (x),
			&width, &height) < 0)
	{
		/* Probably window is not exposed */
		return 1;
	}

	size = select_icon_size (width, height);

	/* This will update width and height to pixmap's width and height */
	image = get_image_data (
			kb->groupNames[kb->curGroup],
			size,
			&width,
			&height);
	if (!image)
	{
		/* Serious problem */
		return -1;
	}

	return xti_draw_icon_from_data (
			x,
			kb->curGroup,
			image,
			width,
			height);
}

int
handle_events (x_t *x, kb_t *kb, uint8_t first_xkb_event, char *bg)
{
	xcb_generic_event_t *e;

	while ((e = xcb_wait_for_event (xti_get_connection (x))))
	{
		switch (e->response_type & ~0x80)
		{
		case XCB_EXPOSE:
		{
			xcb_expose_event_t *ev = (xcb_expose_event_t *)e;

			if (xti_expose_event_handler (x, kb->curGroup, ev))
			{
				if (switch_icon (x, kb) < 0)
				{
					free (e);
					return 1;
				}
			}

			break;
		}

		case XCB_BUTTON_PRESS:
		{
			xcb_button_press_event_t *ev = (xcb_button_press_event_t *)e;

			switch (ev->detail)
			{
			case 3:
				/* Exit on right mouse click */
				free (e);
				return 0;

			default:
				break;

			}

			break;
		}

		case XCB_GRAPHICS_EXPOSURE:
		case XCB_NO_EXPOSURE:
		case XCB_REPARENT_NOTIFY:
		case XCB_MAP_NOTIFY:
			break;

		case XCB_UNMAP_NOTIFY:
		{
			if (!xti_request_dock_timeout (
					xti_get_connection (x),
					xti_get_tray_icon_window (x),
					xti_get_screen_number (x),
					sec))
			{
				return 1;
			}

			xti_draw_icon_from_cache (x, kb->curGroup);

			break;
		}

		default:
			if (e->response_type != first_xkb_event)
			{
				/* Unknown event type, ignore it */
				if (xcb_event_get_label (e->response_type))
				{
					fprintf (
							stderr,
							"Unknown event: %s\n",
							xcb_event_get_label (e->response_type));
				}
				break;
			}

			if (handle_xkb_event (e, kb))
			{
				/*
				 * Size of the status icon did not change,
				 * just switch to the next image.
				 */
				if (switch_icon (x, kb) < 0)
				{
					free (e);
					return 1;
				}
			}

			break;
		}
		/* Free the Generic Event */
		free (e);
	}

	return 1;
}

int
main (int argc, char **argv)
{
	/* keyboard */
	uint8_t		 first_xkb_event;
	/* misc */
	int		 i, ret;

	memset (bg, 0, 8);

	/* Parse command line arguments */
	for (i = 1; i < argc - 1; i++)
	{
		if ((!strcmp (argv[i], "-bg")) && (strlen (argv[i + 1]) == 7))
		{
			strncpy (bg, argv[i + 1], 8);
			bg[8] = '\0';
			i++;
			continue;
		}

		if (!strcmp (argv[i], "-w"))
		{
			if (sscanf (argv[i+1], "%u", &sec) == 1)
			{
				i++;
				continue;
			}
			else
			{
				sec = 0;
			}
		}
	}

	/* Initialize system tray icon */
	x_t *x = xti_initialize_tray_icon (bg, sec, "xkeyboardicon");
	if (!x)
	{
		return 1;
	}

	/* Initialize XKB */
	kb_t *kb = memset (malloc (sizeof (kb_t)), 0, sizeof (kb_t));
	first_xkb_event = init_xkb (
			xti_get_connection (x),
			(xti_get_screen (x))->root,
			kb);
	if (!first_xkb_event)
	{
		free (kb);
		xti_free_and_close_tray_icon (x);
		return 1;
	}

	/* Load and display first icon */
	switch_icon (x, kb);

	ret = handle_events (x, kb, first_xkb_event, bg);

	/* Clean up */
	xti_free_and_close_tray_icon (x);

	for (i = 0; i < LAYOUT_MAX; i++)
	{
		if (kb->groupNames[i] != NULL)
		{
			free (kb->groupNames[i]);
			kb->groupNames[i] = NULL;
		}
	}

	free (kb);

	return ret;
}
