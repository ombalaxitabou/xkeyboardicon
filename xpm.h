/*
 * Load an XPM 3 format image, fill in width, height and image memory, that
 * can be passed to X server, Z pixmap format BGRA
 */

#ifndef XPM_H
#define XPM_H

typedef struct
{
	FILE		*file;		/* xpm image file		*/
	FILE		*x_color_names;	/* rgb.txt			*/
	int		 cpp;		/* chars per pixel		*/
	int		 ncol;		/* number of available colors	*/
	int		 w;		/* width of image		*/
	int		 h;		/* height of image		*/
	char		*bg;		/* background color		*/
	int		 count_colors;	/* yes / no ?			*/
	char		*color_char;	/* color code memory location	*/
	unsigned int	*color_rgba;	/* color values memory location	*/
	unsigned int	*color_count;	/* color counter		*/
} xpm_info_t;

typedef enum
{
	S_UNKNOWN,
	S_SKIP,
	S_HEADER,
	S_COLOR,
	S_IMAGE,
	S_ENDLINE,
	S_ERROR,
} state_t;

int
lookup_color (
		char		*p,	/* pointer to color string	  */
		xpm_info_t	*xpm	/* file info struct		  */
);

state_t
parse_xpm_line (
		char 		 *p,	/* pointer to first char of line   */
		state_t		  last,	/* last state returned by previous
					   call of this function or
					   S_UNKNOWN to initialize         */
		int		 (header_func)(char *, void *),
		int		 (color_func)(char *, void *),
		int		 (image_func)(char *, void *),
		void		 *user_data
);

int
load_xpm_from_memory (
		char		 *xpm_image,	/* XPM3 formatted image in memory	*/
		unsigned char	**x11_image,	/* output, suitable for supplying to X	*/
		xpm_info_t	 *xpm		/* struct that will be filled		*/
);

void
close_and_free (xpm_info_t *xpm);

int
load_xpm (
		unsigned char	**image,		/* image memory location	*/
		int		 *width,		/* width of image		*/
		int		 *height,		/* height of image		*/
		char		 *icon_file_name,
		char		 *background		/* rgb background color */
);

#endif

