
#ifndef HCA_H
#define HCA_H

typedef enum
{
	D_SE, /* Squared Euclidean  */
	D_WE, /* Weighted Euclidean */
	D_MH, /* Manhattan          */
	D_MX, /* Max                */
} distance_t;

unsigned int
calculate_distance (unsigned int *a, unsigned int *b, int len, distance_t d);

#endif
