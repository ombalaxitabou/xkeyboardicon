PATH_PREFIX = /usr/local
PATH_BIN = $(PATH_PREFIX)/bin
PATH_IMAGES = $(PATH_PREFIX)/share/$(bin)/flags

INSTALL = /usr/bin/install
INSTALL_BIN = /usr/bin/install -m 755

# program name
bin = xkeyboardicon
objects = flags.o xkb.o
objects_xcr = xpm.o hca.o

CC = gcc
CFLAGS = -g -Wall
LDFLAGS =

XCB = xcb-image xcb-event xcb-xkb xcb-icccm

# debug
#CFLAGS += -D_DEBUG

CFLAGS += `pkg-config --cflags $(XCB)`
LDFLAGS += `pkg-config --libs $(XCB)` -lxtrayicon -lxpmb

help: commands

## commands: show accepted commands
commands:
	grep -E '^##' Makefile | sed -e 's/## //g'

## all: compile binary
all: $(bin) xcr

## xkeyboardicon: compile xkeyboardicon only
$(bin): xkbi2.c $(objects)
	$(CC) $(CFLAGS) -DICON_PATH='"$(PATH_IMAGES)/%d/%s"' $(LDFLAGS) -o $@ $^

## xcr: compile xpm color reducer
xcr: xcr.c $(objects_xcr)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^

flags.c:
	$(PWD)/flags.sh ./flags

flags.h: flags.c

$(objects): %.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

## install: install xkeyboardicon to dir set in Makefile
install: $(bin)
	$(INSTALL) -d $(PATH_BIN)/
	$(INSTALL) -d $(PATH_IMAGES)/
	$(INSTALL) -d $(PATH_IMAGES)/16/
	$(INSTALL) -d $(PATH_IMAGES)/22/
	$(INSTALL) -d $(PATH_IMAGES)/24/
	$(INSTALL) -d $(PATH_IMAGES)/32/
	$(INSTALL) -d $(PATH_IMAGES)/48/
	$(INSTALL) -d $(PATH_IMAGES)/128/
	$(INSTALL_BIN) -T $(bin) $(PATH_BIN)/$(bin)
	$(INSTALL) flags/16/* $(PATH_IMAGES)/16/
	$(INSTALL) flags/22/* $(PATH_IMAGES)/22/
	$(INSTALL) flags/24/* $(PATH_IMAGES)/24/
	$(INSTALL) flags/32/* $(PATH_IMAGES)/32/
	$(INSTALL) flags/48/* $(PATH_IMAGES)/48/
	$(INSTALL) flags/128/* $(PATH_IMAGES)/128/

## uninstall: remove installed files
uninstall:
	rm -r -f $(PATH_BIN)/$(bin) $(PATH_PREFIX)/share/$(bin)/

## clean: remove compiled files (and not the installed ones)
clean:
	rm -f $(bin) xcr $(objects) $(objects_xcr) flags.c flags.h
