
#ifndef HAVE_XKBI_XKB_H
#define HAVE_XKBI_XKB_H

#define LAYOUT_MAX 4

struct kb_struct {
	uint8_t numGroups;
	uint8_t curGroup;
	char *groupNames[LAYOUT_MAX];
	};

typedef struct kb_struct kb_t;

uint8_t
init_xkb (xcb_connection_t *conn, xcb_window_t root_win, kb_t *kb);

int
handle_xkb_event (xcb_generic_event_t *event, kb_t *kb);

#endif
