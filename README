README

xkeyboardicon is a little flag in systray to show X11 keyboard layout.
It was written in C and uses XCB library.



FEATURES

 * system tray icon showing a flag of current layout
 * change setting by command line arguments
 * no settings window
 * no settings file
 * no autotools



INSTALL

Get source from http://gitlab.com/ombalaxitabou/xkeyboardicon.git
You need libxcb-util0-dev, libxcb-image0-dev libxcb-icccm4-dev
and libxcb-xkb-dev to compile. These are debian packages' names.
Other distributions may have different names for their packages.
Change to xkeyboardicon dir. You may want to change the defaults
in the makefile (notably install dir) and then run :
    $ make xkeyboardicon
    $ su root
    # make install



USAGE

You may run xkeyboardicon every time X starts.
~/.xinitrc is a good place for this.
For example :
    /usr/bin/setxkbmap -layout us,de -option grp:alt_space_toggle
    /usr/local/bin/xkeyboardicon &

This only displays a flag of current keyboard layout, does not change it.
Right click on icon to quit.



XCR UTIL

XPM Color Reducer or 'xcr' is an util to reduce file size of included
flag graphics by reducing the number of colors used.
One color is removed a time from the xpm file.
The color to be removed is chosen based on distance of colors,
following human perception and pixel count:
    First, a colors are searched for the most similar color pair.
    Then, the color with lower pixel count of this pair
    is replaced by the color with higher pixel count.

This way the change may be imperceptible (which is actually the goal),
so file size can reduced with minimal effect on quality of image.
This is only useful for xpm files with low pixel count
and high number of colours (icons).

Open somefile.xpm with xcr:
    $ xcr somefile.xpm

    right / down       reduce number of colors by one
    left  / up         undo last color reduction step
    s                  save file (over write!)
    ESC   / q          quit without saving



BUGS

Please report bugs at https://gitlab.com/ombalaxitabou/xkeyboardicon/issues



CREDITS

2014-2017 Nguyen Dang Peter <ombalaxitabou@users.sourceforge.net>

xkeyboardicon is inspired by fbxkb and originally reused much of its code.
Thanks to Anatoly Asviyan (aka Arsen) and Peter Zelezny who created fbxkb.

