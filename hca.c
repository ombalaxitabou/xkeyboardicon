#include <stdio.h>
#include "hca.h"

unsigned int
calculate_distance (unsigned int *a, unsigned int *b, int len, distance_t d)
{
	int i, red, green, blue, redmean;
	unsigned int dist = 0, ret = 0;

	switch (d)
	{
	case D_SE:
		for (i = 0; i < len; i++)
		{
			if (a[i] > b[i])
				ret += (a[i] - b[i]) * (a[i] - b[i]);
			else
				ret += (b[i] - a[i]) * (b[i] - a[i]);
		}
		break;

	case D_WE:
		/*
		 * Original idea by Thiadmer Riemersma
		 * http://www.compuphase.com/cmetric.htm
		 */
		redmean = ( a[2] + b[2] ) / 2;
		red   = a[2] - b[2];
		green = a[1] - b[1];
		blue  = a[0] - b[0];
		ret = ((512 + redmean) * red * red) >> 8;
		ret += 4 * green * green;
		ret += ((767 - redmean) * blue * blue) >> 8;

	case D_MH:
		for (i = 0; i < len; i++)
		{
			if (a[i] > b[i])
				ret += a[i] - b[i];
			else
				ret += b[i] - a[i];
		}
		break;

	case D_MX:
		for (i = 0; i < len; i++)
		{
			if (a[i] > b[i])
				dist += a[i] - b[i];
			else
				dist += b[i] - a[i];

			if (dist > ret)
				ret = dist;
		}
		break;
	}

	return ret;
}
