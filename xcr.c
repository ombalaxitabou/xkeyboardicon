#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memset () */
#include <errno.h>

#include <xcb/xcb.h>
#include <xcb/xcb_image.h>
#include <xcb/xcb_icccm.h>

#include "xpm.h"
#include "hca.h"

#define DEFAULT_SIZE 16 /* default icon size only used for window creation */

#define BUFFER_MAX 1024
#define UNDO_MAX 256
#define COLOR_MAX 93

#define BACKGROUND "#FFFFFF" /* default icon background color */

struct x_struct {
	xcb_connection_t	 *c;		/* connection			*/
	xcb_screen_t		 *s;		/* screen			*/
	int			 sn;		/* screen number		*/
	xcb_window_t		  win;		/* window			*/
	int			  win_w;	/* window width			*/
	int			  win_h;	/* window height		*/
	int			  win_d;	/* window color depth		*/
	xcb_image_t		 *xim;		/* image			*/
	xcb_pixmap_t		  pm;		/* pixmap			*/
	int			  pm_w;		/* pixmap width			*/
	int			  pm_h;		/* pixmap height		*/
	xcb_gcontext_t		  gc;		/* graphic context		*/
	char			 *fn;		/* file name			*/
	char			 *im[UNDO_MAX];	/* loaded xpm images		*/
	unsigned int		  imcnt;	/* image counter		*/
	unsigned int		  imcur;	/* currently displayed image	*/
	char			  bg[8];
	xpm_info_t		 *xpm;
	};

typedef struct x_struct x_t;

enum action {
	A_NO,
	A_COL,
	A_CPP,
	};

typedef struct {
	char		**image;
	xpm_info_t	 *xpm;
	int		  color;
	int		  row;
	int		  cpp;
	int		  cpp2;
	char		 *rptr;
	char		 *wptr;
	char		 *old;
	char		 *new;
	char		 *cc;
	char		 *cc2;
	enum action	  act;
	} cb_data_t;

int
header_callback (char *rptr, void *p)
{
	cb_data_t *d = (cb_data_t *) p;
	char *wptr = d->wptr;
	int columns, rows, colors, cpp, cpp2;
	int i, c;
	char buffer[BUFFER_MAX];
	char *buff;

	sscanf (rptr, "\"%d %d %d %d", &columns, &rows, &colors, &cpp);

	for (i = 1, c = COLOR_MAX; colors > c; i++, c = c * COLOR_MAX);
	cpp2 = i;

	if ((cpp == 1) || (cpp2 >= cpp))
	{
		sprintf (buffer, "\"%d %d %d %d", columns, rows, (colors - 1), cpp);

		if (d->new)
		{
			d->act = A_COL;
		}
		else
		{
			d->act = A_NO;
		}

		d->cpp  = cpp;
		d->cpp2 = 0;
	}
	else
	{
		sprintf (buffer, "\"%d %d %d %d", columns, rows, (colors - 1), cpp2);

		d->act  = A_CPP;
		d->cpp  = cpp;
		d->cpp2 = cpp2;

		d->cc  = memset (malloc (cpp  * colors), 0, (cpp  * colors));
		d->cc2 = memset (malloc (cpp2 * colors), 0, (cpp2 * colors));

		d->xpm->cpp = cpp;
		d->xpm->ncol = colors;
		d->xpm->color_char = d->cc;
	}

	buff = buffer;
	while (*buff != '\0')
	{
		*wptr = *buff;
		wptr++;
		rptr++;
		buff++;
	}

	while (*rptr != '"') rptr++;

	d->rptr = rptr;
	d->wptr = wptr;

	return 0;
}

int
color_callback (char *rptr, void *p)
{
	cb_data_t *d = (cb_data_t *) p;
	char *wptr = d->wptr;
	int i, c;

	static const char selection[] = " .XoO+@#$%&*=-;:>,<1234567890qwertyuipasdfghjklzxcvbnmMNBVCZASDFGHJKLPIUYTREWQ!~^/()_`'[]{}|";

	if (!strncmp (d->old, rptr + 1, d->cpp))
	{
		while ((*rptr != '\n') && (*rptr != '\0')) rptr++;
		while ((*wptr != '\n') || (*(wptr - 1) == '\n')) wptr--;

		d->rptr = rptr;
		d->wptr = wptr;

		return 0;
	}

	if (d->act != A_CPP) return 0;

	if (*rptr == '"') /* this should always be true */
	{
		if (wptr != rptr) *wptr = *rptr;
		wptr++;
		rptr++;
	}

	for (i = 0; i < d->cpp; i++)
		d->cc[d->color * d->cpp + i] = rptr[i];

	c = d->color;
	switch (d->cpp2)
	{
	case 4: /* 92*92*92 = 778688 */
		*wptr = selection[c / (COLOR_MAX * COLOR_MAX * COLOR_MAX)];
		wptr++;
		c = c % (COLOR_MAX * COLOR_MAX);

	case 3: /* 92*92 = 8464 */
		*wptr = selection[c / (COLOR_MAX * COLOR_MAX)];
		wptr++;
		c = c % (COLOR_MAX * COLOR_MAX);

	case 2:
		*wptr = selection[c / COLOR_MAX];
		wptr++;
		c = c % COLOR_MAX;

	default:
		*wptr = selection[c];
		wptr++;
	}

	for (i = 0; i < d->cpp2; i++)
		d->cc2[d->color * d->cpp2 + i] = wptr[i - d->cpp2];

	rptr += d->cpp;

	d->color++;
	d->rptr = rptr;
	d->wptr = wptr;

	return 0;
}

int image_callback (char *rptr, void *p)
{
	cb_data_t *d = (cb_data_t *) p;
	char *wptr = d->wptr;
	int i, c, byte = 0;

	if (*rptr == '"') /* this should always be true */
	{
		if (wptr != rptr) *wptr = *rptr;
		wptr++;
		rptr++;
	}

//	while (strncmp (rptr, "\",\n", 3) && strncmp (rptr, "\"\n", 2))
	while (*rptr != '"' || (*(rptr + 2) != '\n' && *(rptr + 1) != '\n'))
	{
		switch (d->act)
		{
		case A_COL:
			if ((byte != 0) || (strncmp (d->old, rptr, d->cpp)))
			{
				byte = (byte + 1) % d->cpp;
				if (wptr != rptr) *wptr = *rptr;
				wptr++;
				rptr++;
				break;
			}

			while (byte < d->cpp)
			{
				wptr[byte] = d->new[byte];
				byte++;
			}

			byte = 0;
			rptr += d->cpp;
			wptr += d->cpp;
			break;

		case A_CPP:
			if (!strncmp (d->old, rptr, d->cpp))
				for (byte = 0; byte < d->cpp; byte++)
					rptr[byte] = d->new[byte];

			c = lookup_color (rptr, d->xpm);
			if (c < 0)
				return c;

			for (i = 0; i < d->cpp2; i++)
				wptr[i] = d->cc2[c * d->cpp2 + i];

			rptr += d->cpp;
			wptr += d->cpp2;
			break;

		default:
			return 0;
		}
	}

	d->rptr = rptr;
	d->wptr = wptr;

	return 0;
}

void
replace_color (char **img, char *old, char *new)
{
	state_t	last = S_UNKNOWN;
	cb_data_t data;
	xpm_info_t xpm;

	data.image =  img;
	data.xpm   =  &xpm;
	data.color =  0;
	data.row   =  0;
	data.cpp   =  0;
	data.rptr  = *img;
	data.wptr  = *img;
	data.old   =  old;
	data.new   =  new;
	data.cc    =  NULL;
	data.cc2   =  NULL;

	while (*data.rptr != '\0')
	{
		last = parse_xpm_line (
				data.rptr,
				last,
				header_callback,
				color_callback,
				image_callback,
				&data);

		if (last == S_ERROR)
		{
			if (data.cc)  free (data.cc);
			if (data.cc2) free (data.cc2);
			return;
		}

		while ((*data.rptr != '\n') && (*data.rptr != '\0'))
		{
			if (data.rptr != data.wptr) *data.wptr = *data.rptr;
			data.rptr++;
			data.wptr++;
		}

		while ((*data.rptr == '\n') && (*data.rptr != '\0'))
		{
			if (data.rptr != data.wptr) *data.wptr = *data.rptr;
			data.rptr++;
			data.wptr++;
		}
	}

	*data.wptr = '\0';

	if (data.cc)  free (data.cc);
	if (data.cc2) free (data.cc2);
}

unsigned int *
rgb_to_yuv (
		unsigned int  red,
		unsigned int  green,
		unsigned int  blue,
		unsigned int *ret)
{
	int i;

	ret[0] = 76 * red + 150 * green + 29 * blue;
	ret[1] = 32385 - 43 * red - 84 * green + 127 * blue;
	ret[2] = 32385 + 127 * red - 106 * green - 21 * blue;

	for (i = 0; i < 3; i++)
	{
		if (ret[i] % 255 > 127)
			ret[i] = ret[i] / 255 + 1;
		else
			ret[i] = ret[i] / 255;
	}

	return ret;
}

int
xpm_reload (
		xpm_info_t	 *xpm,
		char		 *xpm_img,
		unsigned char	**image,
		char		 *background,
		int		  count)
{
	int ret;

	close_and_free (xpm);
	free (*image);
	*image = NULL;

	xpm = memset (xpm, 0, sizeof (xpm_info_t));
	xpm->count_colors = count;
	xpm->bg = background;

	ret = load_xpm_from_memory (xpm_img, image, xpm);
	if (ret < 0)
	{
		if (*image)
		{
			free (*image);
			*image = NULL;
		}
		close_and_free (xpm);
		return ret;
	}

	return 0;
}

void
display_window_title (x_t *x, xpm_info_t *xpm)
{
	char title[BUFFER_MAX];

	snprintf (title, BUFFER_MAX, "xcr - %s (%d colors, %d characters per pixel)", x->fn, xpm->ncol, xpm->cpp);

	xcb_icccm_set_wm_name(
			x->c,
			x->win,
			XCB_ATOM_STRING,
			8,
			strlen (title),
			title);
}

xcb_image_t *
load_image (x_t *x)
{
	unsigned char	*image = NULL;
	xcb_image_t	*ximage = NULL;
	xpm_info_t	*xpm = NULL;
	char		 buffer[BUFFER_MAX];
	char		*p;
	int		 i, l, b, ret;

	/* Only work on one image so counter must be zero at this time */
	if (x->imcnt)
		return NULL;

	xpm = memset (x->xpm, 0, sizeof (xpm_info_t));
	xpm->count_colors = 1;
	xpm->bg = x->bg;

	xpm->file = fopen (x->fn, "r");
	if (!xpm->file)
	{
		fprintf (stderr, "Cannot open image file\n");
		return NULL;
	}

	for (i = 0, l = 0; fgets (buffer, BUFFER_MAX, xpm->file); i++)
	{
		b = strlen (buffer);
		p = realloc (x->im[x->imcnt], l + b);
		if (!p)
		{
			fprintf (stderr, "Cannot allocate sufficient memory\n");
			return NULL;
		}
		x->im[x->imcnt] = p;
		memcpy (x->im[x->imcnt] + l, buffer, b);
		l += b;
	}

	x->imcnt++;

	if (load_xpm_from_memory (x->im[x->imcur], &image, xpm) < 0)
	{
		if (image) free (image);
		close_and_free (xpm);
		return NULL;
	}

	/* Remove not used colors */
	for (i = 0, l = 0; i < xpm->ncol; i++)
		if (xpm->color_count[i] == 0)
		{
			replace_color (	&x->im[x->imcur],
					&xpm->color_char[xpm->cpp * i],
					NULL);

			if ((xpm->ncol - l) % COLOR_MAX == 0)
			{
				ret = xpm_reload (
						xpm,
						x->im[x->imcur],
						&image,
						x->bg,
						1);

				if (ret < 0) return NULL;

				i = -1;
			}

			l++;
		}


	if (xpm_reload (xpm, x->im[x->imcur], &image, x->bg, 0) < 0)
		return NULL;

	x->pm_w = xpm->w;
	x->pm_h = xpm->h;

	ximage = xcb_image_create_native (
			x->c,
			x->pm_w,
			x->pm_h,
			XCB_IMAGE_FORMAT_Z_PIXMAP,
			x->win_d,
			image,
			4 * x->pm_w * x->pm_h * sizeof (unsigned char),
			image);

	display_window_title (x, xpm);
	close_and_free (xpm);

	return ximage;
}

void
draw (x_t *x)
{
	xcb_image_put (x->c, x->pm, x->gc, x->xim, 0, 0, 0);
	xcb_copy_area (x->c, x->pm, x->win, x->gc, 0, 0, 0, 0, x->pm_w, x->pm_h);

	xcb_flush (x->c);
}

int
display_image (x_t *x)
{
	unsigned char	*image = NULL;
	xcb_image_t	*ximage2 = NULL;
	xpm_info_t	*xpm = memset (x->xpm, 0, sizeof (xpm_info_t));

	xpm->count_colors = 0;
	xpm->bg = x->bg;

	if (load_xpm_from_memory (x->im[x->imcur], &image, xpm) < 0)
	{
		if (image) free (image);
		close_and_free (xpm);
		return -1;
	}

	x->pm_w = xpm->w;
	x->pm_h = xpm->h;

	ximage2 = xcb_image_create_native (
			x->c,
			x->pm_w,
			x->pm_h,
			XCB_IMAGE_FORMAT_Z_PIXMAP,
			x->win_d,
			image,
			4 * x->pm_w * x->pm_h * sizeof (unsigned char),
			image);

	display_window_title (x, xpm);
	close_and_free (xpm);

	if (!ximage2)
	{	/* some error happened, preserve original pixmap */
		return -1;
	}

	xcb_image_destroy (x->xim);
	x->xim = ximage2;
	draw (x);

	return 0;
}

int
undo (x_t *x)
{
	if (x->imcnt == 1)
	{	/* nothing to undo */
		return 0;
	}

	x->imcnt--;

	if (x->imcur == 0)
	{	/* there are some overwritten states */
		x->imcur = UNDO_MAX - 1;
	}
	else
	{
		x->imcur--;
	}

	return display_image (x);
}

int
reduce (x_t *x)
{
	unsigned char		*image = NULL;
	xpm_info_t		*xpm = NULL;
	int			 i, j, k, mx_max, mx_pos, mx_min_diff_pos;
	unsigned int		*distance_matrix = NULL;
	unsigned int		 distance_min = 0;
	unsigned int		 i_color[4], j_color[4];

	xpm = memset (x->xpm, 0, sizeof (xpm_info_t));
	xpm->count_colors = 1;
	xpm->bg = x->bg;

	/*
	 * Count colors
	 */
	if (load_xpm_from_memory (x->im[x->imcur], &image, xpm) < 0)
	{
		if (image) free (image);
		close_and_free (xpm);
		return -1;
	}

	if (xpm->ncol < 2)
	{
		close_and_free (xpm);
		free (image);
		return 0;
	}

	/*
	 * Add to undo history
	 */
	x->im[(x->imcur + 1) % UNDO_MAX] = strdup (x->im[x->imcur]);
	x->imcur = (x->imcur + 1) % UNDO_MAX;
	if (x->imcnt < UNDO_MAX) x->imcnt++;

	/*
	 * Create and fill in distance matrix
	 */
	mx_max = (xpm->ncol * (xpm->ncol - 1)) / 2;
	distance_matrix = memset (
			malloc (mx_max * sizeof (unsigned int)),
			0,
			mx_max * sizeof (unsigned int));

	mx_pos = 0;
	for (i = 0; i < (4 * xpm->ncol); i += 4)
	{
		for (j = i + 4; j < (4 * xpm->ncol); j += 4)
		{	/* mx_pos = j - i - 1 + i * xpm->ncol - (i * (i + 1)) / 2; */

#if 1
			rgb_to_yuv (
					xpm->color_rgba[i + 2],
					xpm->color_rgba[i + 1],
					xpm->color_rgba[i + 0],
					i_color);
			rgb_to_yuv (
					xpm->color_rgba[j + 2],
					xpm->color_rgba[j + 1],
					xpm->color_rgba[j + 0],
					j_color);
			i_color[3] = xpm->color_rgba[i + 3];
			j_color[3] = xpm->color_rgba[j + 3];
#else
			for (k = 0; k < 4; k++)
			{
				i_color[k] = xpm->color_rgba[i + k];
				j_color[k] = xpm->color_rgba[j + k];
			}
#endif
			distance_matrix[mx_pos++] = calculate_distance (
					i_color,
					j_color,
					4,
					D_SE);
		}
	}

	/*
	 * Find closest colors
	 */
	mx_min_diff_pos = 0;
	distance_min = distance_matrix[0];
	for (mx_pos = 1; mx_pos < mx_max; mx_pos++)
	{
		if (distance_matrix[mx_pos] < distance_min)
		{
			distance_min = distance_matrix[mx_pos];
			mx_min_diff_pos = mx_pos;
		}
	}

	for (i = 0; (i + 1) * (xpm->ncol - 1) - (i * (i + 1)) / 2 - 1 < mx_min_diff_pos; i++);
	j = mx_min_diff_pos - i * (xpm->ncol - 1) + (i * (i - 1)) / 2 + 1 + i;

	/*
	 * Replace least used color with closest one
	 */
	if (xpm->color_count[i] > xpm->color_count[j])
	{	/* Swap i and j */
		k = i;
		i = j;
		j = k;
	}

	replace_color (	&x->im[x->imcur],
			&xpm->color_char[xpm->cpp * i],
			&xpm->color_char[xpm->cpp * j]);

	/* Clean up */
	free (distance_matrix);
	close_and_free (xpm);
	free (image);
	image = NULL;

	return display_image (x);
}

int
save (x_t *x)
{
	char *p;
	FILE *f;

	f = fopen (x->fn, "w");
	if (!f) return -1;

	for (p = x->im[x->imcur]; *p != '\0'; p++) fputc (*p, f);

	return 0;
}

int
create_pixmap_with_icon (x_t *x)
{
	uint32_t	 mask = 0;
	uint32_t	 values[2];

	/* Load image */
	x->xim = load_image (x);
	if (!x->xim)
	{
		xcb_free_gc (x->c, x->gc);
		if (!xcb_connection_has_error (x->c))
		{
			xcb_disconnect (x->c);
		}
		return -1;
	}

	/* Create backing pixmap */
	x->pm = xcb_generate_id (x->c);
	xcb_create_pixmap (x->c, x->win_d, x->pm, x->win, x->pm_w, x->pm_h);

	/* Create pixmap plot gc */
	mask = XCB_GC_FOREGROUND | XCB_GC_BACKGROUND;
	values[0] = x->s->black_pixel;
	values[1] = x->s->white_pixel;

	/* Create graphic context */
	x->gc = xcb_generate_id (x->c);
	xcb_create_gc (x->c, x->gc, x->pm, mask, values);

	/* Draw image on surface */
	draw (x);

	return 0;
}

void
destroy_pixmap_with_icon (x_t *x)
{
	int i;

	xcb_free_gc (x->c, x->gc);
	xcb_free_pixmap (x->c, x->pm);
	xcb_image_destroy (x->xim); /* this also frees image data */

	/* free undo history */
	for (i = 0; i < x->imcnt; i++)
	{
		free (x->im[i]);
	}
	x->imcnt = 0;
}

void
get_window_size (x_t *x)
{
	/* Find out geometry of window */
	xcb_get_geometry_cookie_t cookie = xcb_get_geometry (x->c, x->win);
	xcb_get_geometry_reply_t *geo = xcb_get_geometry_reply (x->c, cookie, NULL);
	x->win_d = geo->depth;
	x->win_w = geo->width;
	x->win_h = geo->height;
	free (geo);
}

int
handle_events (x_t *x)
{
	xcb_generic_event_t *e;

	while ((e = xcb_wait_for_event (x->c)))
	{
		switch (e->response_type & ~0x80)
		{
		case XCB_EXPOSE: {
			xcb_expose_event_t *ev = (xcb_expose_event_t *)e;

			xcb_copy_area (x->c, x->pm, x->win, x->gc,
					ev->x, ev->y,
					ev->x, ev->y,
					ev->width, ev->height);

			xcb_flush (x->c);
			break;
		}

		case XCB_BUTTON_PRESS: {
			xcb_button_press_event_t *ev = (xcb_button_press_event_t *)e;

			switch (ev->detail)
			{
			/* left mouse button */
			case 1:
				if (reduce (x))
				{
					free (e);
					return -1;
				}
				break;
			/* right mouse button */
			case 3:
				if (undo (x))
				{
					free (e);
					return -1;
				}
				break;

			default:
				break;

			}

			break;
		}

		case XCB_KEY_PRESS: {
			xcb_key_release_event_t *ev = (xcb_key_release_event_t *)e;

			switch (ev->detail)
			{
			/* ESC */
			case 9:
			/* Q */
			case 24:
				free (e);
				return 0;
			/* UP */
			case 111:
			/* LEFT */
			case 113:
				if (undo (x))
				{
					free (e);
					return -1;
				}
				break;
			/* DOWN */
			case 116:
			/* RIGHT */
			case 114:
				if (reduce (x))
				{
					free (e);
					return -1;
				}
				break;
			/* S */
			case 39:
				if (save (x))
				{
					free (e);
					return -1;
				}
				break;
			default:
				break;
			}
			break;
		}

		default:
			break;
		}
		/* Free the Generic Event */
		free (e);
	}

	return -1;
}

int
main (int argc, char **argv)
{
	uint32_t	 mask = 0;
	uint32_t	 values[2];
	int		 ret;

	/*
	 * There must be one and only one command line argument,
	 * which should be path and file name of an xpm image.
	 */
	if (argc != 2) return -1;

	x_t *x = memset (malloc (sizeof (x_t)), 0, sizeof (x_t));
	x->fn = strdup (argv[1]);
	x->xpm = memset (malloc (sizeof (xpm_info_t)), 0, sizeof (xpm_info_t));

	strncpy (x->bg, BACKGROUND, 7);

	/*
	 * Open the connection to the X server.
	 * Use the DISPLAY environment variable as the default display name
	 */
	x->c = xcb_connect (NULL, NULL);
	if (xcb_connection_has_error (x->c))
	{
		fprintf (stderr, "Could not open display\n");
		return 1;
	}

	/* Get the first screen */
	x->s = xcb_setup_roots_iterator (xcb_get_setup (x->c)).data;

	/* Ask for our window's Id */
	x->win = xcb_generate_id (x->c);

	/* Create the window */
	mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
	values[0] = x->s->white_pixel;
	values[1] = 	XCB_EVENT_MASK_EXPOSURE		|
			XCB_EVENT_MASK_BUTTON_PRESS	|
			XCB_EVENT_MASK_BUTTON_RELEASE	|
			XCB_EVENT_MASK_KEY_PRESS	|
			XCB_EVENT_MASK_KEY_RELEASE;

	xcb_create_window (
			x->c,				/* Connection		*/
			XCB_COPY_FROM_PARENT,		/* depth (same as root) */
			x->win,				/* window Id		*/
			x->s->root,			/* parent window	*/
			0, 0,				/* x, y			*/
			DEFAULT_SIZE, DEFAULT_SIZE,	/* width, height	*/
			0,				/* border_width		*/
			XCB_WINDOW_CLASS_INPUT_OUTPUT,	/* class		*/
			x->s->root_visual,		/* visual		*/
			mask, values);			/* masks		*/

	/* Map the window on the screen */
	xcb_map_window (x->c, x->win);

	get_window_size (x);
	ret = create_pixmap_with_icon (x);
	if (ret)
	{
		free (x->fn);
		free (x);
		return ret;
	}

	ret = handle_events (x);

	/* Clean up */
	destroy_pixmap_with_icon (x);
	if (!xcb_connection_has_error (x->c))
	{
		xcb_disconnect (x->c);
	}

	free (x->xpm);
	free (x->fn);
	free (x);

	return ret;
}
