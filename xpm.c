#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memset () */
#include <errno.h>
#include "xpm.h"

#define BUFFER_MAX 1000

#define COLOR_OPAQUE 255
#define COLOR_TRANSPARENT 0

#define SHARED_DIR "/home/peter/Development/xkeyboardicon"
#define COLOR_NAMES_FILE "rgb.txt"

typedef struct
{
	unsigned char	**image;
	xpm_info_t	 *xpm;
	int		  color;
	int		  row;
} callback_data_t;

/*
 * Parse header, fill in various information on provided image file (see
 * variables for details), allocate memory for later use.
 */
int
parse_header (
		char		 *p,		/* pointer to header string	*/
		xpm_info_t	 *xpm,		/* file info struct		*/
		unsigned char	**image)	/* image memory location	*/
{

	errno = 0;
	sscanf (p, "%d %d %d %d", &xpm->w, &xpm->h, &xpm->ncol, &xpm->cpp);
	if (errno)
	{
		fprintf (stderr, "Could not parse icon file\n");
		return -1;
	}

	xpm->color_char = malloc (xpm->ncol * xpm->cpp * sizeof (unsigned char));
	if (!xpm->color_char)
	{
		fprintf (stderr, "Could allocate memory\n");
		return -1;
	}

	xpm->color_rgba = malloc (xpm->ncol * 4 * sizeof (unsigned int));
	if (!xpm->color_rgba)
	{
		free (xpm->color_char);
		fprintf (stderr, "Could allocate memory\n");
		return -1;
	}

	if (xpm->count_colors)
	{
		xpm->color_count = memset (
				malloc (xpm->ncol * sizeof (unsigned int)),
				0,
				(xpm->ncol * sizeof (unsigned int)));
		if (!xpm->color_count)
		{
			free (xpm->color_char);
			free (xpm->color_rgba);
			fprintf (stderr, "Could allocate memory\n");
			return -1;
		}
	}

	*image = memset (
			malloc (4 * xpm->w * xpm->h * sizeof (unsigned char)),
			0,
			4 * xpm->w * xpm->h * sizeof (unsigned char));
	if (!*image)
	{
		free (xpm->color_char);
		free (xpm->color_rgba);
		if (xpm->count_colors) free (xpm->color_count);
		fprintf (stderr, "Could allocate memory\n");
		return -1;
	}

	return 0;
}

void
get_color (
		char		*color_string,
		int		 length,	/* length of color string	*/
		xpm_info_t	*xpm,
		unsigned int	*rgba)		/* copy identified color vaules
						   to this memory location	*/
{
	char buffer[BUFFER_MAX];

	/*
	 * Open file if not already opened.
	 * Since we do not know, if this file is needed later,
	 * we do not close it. That must be done outside this function.
	 */
	if (!xpm->x_color_names)
	{
		sprintf (
			buffer,
			"%s/%s",
			SHARED_DIR,
			COLOR_NAMES_FILE);

		xpm->x_color_names = fopen (buffer, "r");

		if (!xpm->x_color_names)
		{
			fprintf (stderr, "Cannot open icon file\n");
			return;
		}
	}
	else
	{
		/* start from the beginning not to miss any names */
		rewind (xpm->x_color_names);
	}

	while (fgets (buffer, BUFFER_MAX, xpm->x_color_names))
	{
		char *p = buffer;

		/* comment in rgb.txt */
		if (*p == '!') continue;

		/* find first character of color name string */
		while (((*p >= '0') && (*p <= '9')) ||
				(*p == ' ') ||
				(*p == '	'))
			p++;

		/* check length of color name string */
		if (*(p + length) != '\n') continue;

		/* compare strings */
		if (strncmp (p, color_string, length)) continue;

		/* found a matching color name */
		sscanf (buffer, "%3d %3d %3d", rgba, rgba + 1, rgba + 2);
		*(rgba + 3) = COLOR_OPAQUE;
		return;
	}

	/* set black as default */
	*rgba       = 0;
	*(rgba + 1) = 0;
	*(rgba + 2) = 0;
	*(rgba + 3) = COLOR_OPAQUE;
}

int
parse_color (
		char		*p,		/* pointer to color string	*/
		xpm_info_t	*xpm,
		char		*code,		/* pointer to memory location,
						   where color code should be
						   copied to			*/
		unsigned int	*rgba)		/* copy identified color vaules
						   to this memory location	*/
{
	int i;

	/* color name */
	for (i = 0; i < xpm->cpp; i++)
	{
		*(code + i) = *p++;
	}

	/* check supported feature */
	while (*p == ' ') p++;
	if (*p != 'c')
	{
		fprintf (stderr, "\
Not supported XPM file.\n\
Please report bug at gitlab.com/ombalaxitabou/xkeyboardicon and\n\
attach problematic file to report.\n\
Thanks.\n");
		return -1;
	}

	/* color value */
	for (p++; *p == ' '; p++);
	for (i = 0; *(p + i) != '"'; i++);

	if (*p == '#')
	{
		/* convert hexa color */
		sscanf (p, "#%2x%2x%2x", rgba, rgba + 1, rgba + 2);
		*(rgba + 3) = COLOR_OPAQUE;
		return 0;
	}


	/* 'None' for transparent */
	if (!strncmp (p, "None", 4))
	{
		if (!xpm->bg)
		{
			/* set transparent black as default */
			*rgba       = 0;
			*(rgba + 1) = 0;
			*(rgba + 2) = 0;
			*(rgba + 3) = COLOR_TRANSPARENT;
			return 0;
		}

		sscanf (xpm->bg, "#%2x%2x%2x", rgba, rgba + 1, rgba + 2);
		*(rgba + 3) = COLOR_TRANSPARENT;
		return 0;
	}

	/* lookup color from rgb.txt */
	get_color (p, i, xpm, rgba);

	return 0;
}

int
lookup_color (
		char		*p,	/* pointer to color string	  */
		xpm_info_t	*xpm)	/* file info struct		  */
{
	int	 i, j;
	char	*t;		/* pointer to color code position */

	t = xpm->color_char;
	i = 0;
	j = 0;
	while ((j < xpm->cpp) && (i < xpm->ncol))
	{
		/* matching char in color code, check next char, if any */
		if (*(t + j) == *(p + j))
		{
			j++;
			continue;
		}

		/* no match, go to next color */
		i++;                   /* next color number           */
		t += xpm->cpp;         /* pointer to next color code  */
		j = 0;                 /* char position in color code */
	}

	/* found correct one */
	if (j == xpm->cpp)
	{
		return i;
	}

	return -1;
}

int
fill_image_row (
		char		*p,	/* pointer to color string	  */
		xpm_info_t	*xpm,	/* file info struct		  */
		unsigned char	*q)	/* pointer to image data position */
{
	int i;

	/* walk through the line */
	for (; *p != '"'; p += xpm->cpp)
	{
		i = lookup_color (p, xpm);

		if ((i < 0) || (i > xpm->ncol))
		{
			/* could not find color */
			fprintf (stderr, "Error ocurred when filling image data\n");
			fprintf (stderr, "Could not find color: \"");
			for (i = 0; i < xpm->cpp; i++) fprintf (stderr, "%c", p[i]);
			fprintf (stderr, "\"\n");
			return -1;
		}

		if (xpm->count_colors)
		{
			xpm->color_count[i]++;
		}

		*q++ = *(xpm->color_rgba + 4 * i + 2);
		*q++ = *(xpm->color_rgba + 4 * i + 1);
		*q++ = *(xpm->color_rgba + 4 * i + 0);
		*q++ = *(xpm->color_rgba + 4 * i + 3);
	}

	return 0;
}

void
close_and_free (xpm_info_t *xpm)
{
	if (!xpm)
	{
		return;
	}

	if (xpm->x_color_names)
	{
		fclose (xpm->x_color_names);
		xpm->x_color_names = NULL;
	}

	if (xpm->file)
	{
		fclose (xpm->file);
		xpm->file = NULL;
	}

	if (xpm->color_char)
	{
		free (xpm->color_char);
		xpm->color_char = NULL;
	}

	if (xpm->color_rgba)
	{
		free (xpm->color_rgba);
		xpm->color_rgba = NULL;
	}

	if (xpm->color_count)
	{
		free (xpm->color_count);
		xpm->color_count = NULL;
	}
}

int
header_cb (char *p, void *u)
{
	callback_data_t *d = (callback_data_t *) u;

	return parse_header (p + 1, d->xpm, d->image);
}

int
color_cb (char *p, void *u)
{
	int r;
	callback_data_t *d = (callback_data_t *) u;
	xpm_info_t *xpm = d->xpm;

	r = parse_color (
			p + 1,
			xpm,
			xpm->color_char + (d->color * xpm->cpp),
			xpm->color_rgba + (4 * d->color));

	d->color++;

	return r;
}

int
image_cb (char *p, void *u)
{
	int r;
	callback_data_t *d = (callback_data_t *) u;

	r = fill_image_row (p + 1, d->xpm, *(d->image) + (d->row * 4 * d->xpm->w));

	d->row++;

	return r;
}

state_t
parse_xpm_line (
		char 		 *p,
		state_t		  last,
		int		 (header_func)(char *, void *),
		int		 (color_func)(char *, void *),
		int		 (image_func)(char *, void *),
		void		 *user_data)
{
	static int ncols = 0, nrows = 0, colors = 0, color = 0;
	state_t	state;
	int	ret;

	if (last == S_UNKNOWN)
	{
		ncols  = 0;
		nrows  = 0;
		colors = 0;
		color  = 0;
	}

	state = S_UNKNOWN;
	while (state != S_ENDLINE)
	{
		switch (state)
		{
		case S_HEADER:
			sscanf (p, "\"%d %d %d ", &ncols, &nrows, &colors);

			ret = header_func (p, user_data);
			if (ret < 0)
				return S_ERROR;

			state = S_ENDLINE;
			last = S_HEADER;

			break;

		case S_COLOR:
			ret = color_func (p, user_data);
			if (ret < 0)
				return S_ERROR;

			state = S_ENDLINE;
			last = S_COLOR;

			color++;
			break;

		case S_IMAGE:
			ret = image_func (p, user_data);
			if (ret < 0)
				return S_ERROR;

			state = S_ENDLINE;
			last = S_IMAGE;

			break;

		default:
			if (*p != '"')
			{
				state = S_ENDLINE;
				break;
			}

			switch (last)
			{
			case S_HEADER:
				state = S_COLOR;
				break;

			case S_COLOR:
				if (color == colors)
				{
					state = S_IMAGE;
					break;
				}
				state = S_COLOR;
				break;

			case S_IMAGE:
				state = S_IMAGE; /* final state */
				break;

			default:
				state = S_HEADER;
				break;
			}

			break;
		}
	}

	return last;
}

int
load_xpm_from_memory (char *xpm_img, unsigned char **image, xpm_info_t *xpm)
{
	/* XPM 3 format */
	state_t last = S_UNKNOWN;
	char *p = xpm_img;
	callback_data_t cb_data;

	cb_data.image = image;
	cb_data.xpm   = xpm;
	cb_data.color = 0;
	cb_data.row   = 0;

	while (*p != '\0')
	{
		last = parse_xpm_line (
				p,
				last,
				header_cb,
				color_cb,
				image_cb,
				&cb_data);

		if (last == S_ERROR)
		{
			close_and_free (xpm);
			return -1;
		}

		while ((*p != '\n') && (*p != '\0')) p++;
		while ((*p == '\n') && (*p != '\0')) p++;
	}

	return 0;
}

int
load_xpm (
		unsigned char	**image,		/* image memory location	*/
		int		 *width,		/* width of image		*/
		int		 *height,		/* height of image		*/
		char		 *icon_file_name,
		char		 *background)
{
	char		 buffer[BUFFER_MAX];
	callback_data_t	 cb_data;
	xpm_info_t	*xpm;

	xpm = memset (malloc (sizeof (xpm_info_t)), 0, sizeof (xpm_info_t));

	/* load file here, find out chars per pixel, width, height, etc. */
	xpm->file = fopen (icon_file_name, "r");
	if (!xpm->file)
	{
		fprintf (stderr, "Cannot open icon file\n");
		return -1;
	}

	xpm->bg = background;

	cb_data.image = image;
	cb_data.xpm   = xpm;
	cb_data.color = 0;
	cb_data.row   = 0;

	/* XPM 3 format */
	state_t last = S_UNKNOWN;
	while (fgets (buffer, BUFFER_MAX, xpm->file))
	{
		last = parse_xpm_line (
				buffer,
				last,
				header_cb,
				color_cb,
				image_cb,
				&cb_data);

		if (last == S_ERROR)
		{
			close_and_free (xpm);
			free (xpm);
			return -1;
		}
	}

	*width = xpm->w;
	*height = xpm->h;

	close_and_free (xpm);
	free (xpm);

	return 0;
}
